<?php

define('MYSQL_RES_ROW_COL', 1);
define('MYSQL_RES_COL_ROW', 2);
define('MYSQL_RES_ROW', 3);
define('MYSQL_RES_VAL', 4);
define('MYSQL_RES_KEY_VAL', 5);
define('MYSQL_RES_COL', 0);

class DB{ // MySQLi

	protected $conn, $res;
	protected static $instance = false; //The single instance
	protected static $transaction_mode = false;
	
	protected function msgDisconnected(){
        Debug::log('Disconnected', 'error_db');
        //ControllerStd::showProblem();
        exit();
	}

	// Constructor
	protected function __construct() {
		$this->conn = @mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_DB) or $this->msgDisconnected();		
        $this->conn->query('set names `utf8`');
	}

	// Magic method clone is empty to prevent duplication of connection
	protected function __clone() {

	}

	// Get instance
	public static function getInstance() {
		if(!self::$instance) { // If no instance then make one
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public static function close(){
        self::getInstance()->close();
	}

	// Execute query
	public function query($query){	
        $this->res = $this->conn->query($query);
        $err = $this->getErrNo();
        if ($err > 0){
            Debug::log("Err ($err): $query", 'error_db');
            if (self::$transaction_mode){
                $this->rollback();
            }
        }
        else{
            return $this->res;
        }
	}

	// Alternative of "query"
	public function execute($query){
		return $this->query($query);
	}
	
	public function escape($data){
        if (is_array($data)){
            foreach($data as $k => $v){
                $data[$k] = $this->conn->real_escape_string($v);
            }
        }
        else{
            $data = $this->conn->real_escape_string($data);
        }
        return $data;
	}
	
	public function insertSome2Many($table, $col1, $col2){ // one2many and many2many
	    $col1 = (array)$col1; // force cast to array
	    $col2 = (array)$col2; // force cast to array
	    $n1 = count($col1);
	    $n2 = count($col2);

	    if (($n1 == 0 && $n2 == 0) || ($n1 != $n2 && $n1 > 1)){ // if wrong structure
	        if (self::$transaction_mode){
	            Debug::log('Err (insertSome2Many): Wrong structure', 'error_db');
	            $this->rollback();
	        }
	        return false;
	    }	    
	    $query = "insert ignore into `$table` values";
	    for($i = 0; $i < $n2; $i++){
	        if (empty($col1[$i])){ // if col1 is shorten than col2
	            $col1[$i] = $col1[0];
	        }
	        $query .= "('" . addslashes($col1[$i]) . "','" . addslashes($col2[$i]) . "')";
	        if ($i != $n2 - 1){ // if it's not the last
	            $query .= ',';
	        }
	    }
	    $this->query($query);
	    return true;
	}
	
	public function queryGetArray($query, $mode = 1){
	    $this->query($query);
	    return $this->getArrayFromRes($mode);
	}

	public function lock($tables, $rw = 'write'){
		$query = "lock tables ";
		if (is_array($tables)){
			foreach($tables as $k => $table){
                if (is_array($rw)){
                    $query .= $table . ' ' . $rw[$k] . ', ';
                }
                else{
                    $query .= $table . ' ' . $rw . ', ';
                }
			}
			$query = preg_replace('@,\s+$@', '', $query);
		}
		else{
			$query .= $tables . ' ' . $rw;
		}
		$this->query($query);
	}

	public function unlock(){
		$query = 'unlock tables';
		$this->query($query);
	}

	public function startTransaction($isolation_level = ''){
	    if (self::$transaction_mode){
	        Debug::log('Trying to start transaction when it\'s already started', 'error_db');
	        return false;
	    }
	    self::$transaction_mode = true;
        $query = 'set autocommit=0';
        $this->query($query);
		$query = 'start transaction';
		if (!empty($isolation_level)){
			$query .= ' isolation level ' . $isolation_level;
		}
		$this->query($query);
	}

	public function commit(){
	    if (!self::$transaction_mode){
	        Debug::log('Trying to commit without transaction started', 'error_db');
	        return false;
	    }
	    if ($this->getErrNo() == 0){ // if all are OK
		  $query = 'commit';
		  $this->query($query);
		  $query = 'set autocommit=1';
		  $this->query($query);
		  self::$transaction_mode = false;
		  return true;
	    }
	    // else (something wrong)
	    Debug::log('Unsuccessful commit: ' . $this->getErrNo(), 'error_db');
        $this->rollback();
        return false;
	}

	public function rollback(){
	    if (!self::$transaction_mode){
	        Debug::log('Trying to rollback without transaction started', 'error_db');
	        return false;
	    }
		$query = 'rollback';
		$this->query($query);
        $query = 'set autocommit=1';
        $this->query($query);
        self::$transaction_mode = false;
	}

	public function getInsertID(){
		return (int)@$this->conn->insert_id;
	}

	public function getNumRows($res = ''){
	    if ($res == ''){
	        $res = $this->res;
	    }
		return (int)@mysqli_num_rows($res);
	}

	public function getAffectedRows(){
	    return (int)@$this->conn->affected_rows;
	}

	public function getErrNo(){
		return $this->conn->errno;
	}

	public function getArrayFromRes($mode = 1, $res = ''){
		#If invalid mode then default mode
		if (($mode != 0) && ($mode != 1) && ($mode != 2) && ($mode != 3) && ($mode != 4) && ($mode != 5)){
			$mode = 1;
		}
		if ($res == ''){
		    $res = $this->res;
		}
		$i = 0;
		$return = array();
		if ($this->getNumRows($res) > 0){
            while ($row = $res->fetch_assoc()) {
                foreach($row as $key => $val){
                    if ($mode == 1){ //default
                        $return[$i][$key] = $val;
                    }
                    elseif ($mode == 2){
                        $return[$key][$i] = $val;
                    }
                    elseif ($mode == 3){
                        $return[$key] = $val;
                    }
                    elseif ($mode == 4){
                        $return = $val;
                    }
                    elseif ($mode == 5){ // can be used only if there are only two selected columns; first:key, second:val
                        if (empty($return)){ //if first loop
                            $return[$val] = '';
                        }
                        else{
                            $return[key($return)] = $val;
                        }
                    }
                    else{ //mode '0'
                        $return[$i] = $val;
                    }
                }
                $i ++;
            }
        }
		return $return;
	}

}
?>
