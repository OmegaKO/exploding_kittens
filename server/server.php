<?php
    require 'vendor/autoload.php';
    require('config.php');

    #Main autoload
    spl_autoload_register(function($className){
        $clsName = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        $path = 'lib' . DIRECTORY_SEPARATOR . $clsName . '.php';
        if (is_file($path)){
            include($path);
        }
        #Try to start init() if exist
        try{
            $rf = new ReflectionMethod($clsName, 'init');
            $rf->invoke(null);
        }
        catch(Exception $e){
            // do nothing
        }
    });

    // Run the server application through the WebSocket protocol on port 8080
    $app = new Ratchet\App('localhost', 8000);
    $app->route('/', new Srv, array('*'));    
    echo("Server started at localhost:8000\n");    
    $app->run();
?>
