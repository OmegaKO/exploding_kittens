<?php
	class Files{

		public static function loadFile($f_name){
            if (!file_exists($f_name)){
                Debug::log('File \'' . $f_name . '\' not found', 'files_err');
                return false;
			}
			$f = @fopen($f_name, 'r');
			$return = @fread($f,filesize($f_name));
			@fclose($f);
			return $return;
		}

		public static function saveFile($f_name, $content, $append = false){
			if ($append){
				$method = 'a';
			}
			else{
				$method = 'w';
			}
			$f = @fopen($f_name, $method);
			@fwrite($f, $content);
			@fclose($f);
		}

	}
?>
