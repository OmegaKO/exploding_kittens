<?php
    use Ratchet\MessageComponentInterface;
    use Ratchet\ConnectionInterface;

    class Srv implements MessageComponentInterface {
        protected $clients;

        public function __construct() {
            $this->clients = new \SplObjectStorage;
        }

        public function onOpen(ConnectionInterface $conn) {
            $this->clients->attach($conn);
        }

        public function onMessage(ConnectionInterface $connection, $data_str) {
            $data = (array)@json_decode($data_str, true);
            switch((string)@$data['cmd']){
                case 'registration': $res_arr = Cmd::registration((string)@$data['username'], (string)@$data['password'], (string)@$data['email'], (int)@$connection->user_id); break;
                case 'login': $res_arr = Cmd::login((string)@$data['username'], (string)@$data['password'], (int)@$connection->user_id); break;
                case 'password_recovery': $res_arr = Cmd::passwordRecovery((string)@$data['email'], (int)@$connection->user_id); break;
                case 'create_game': $res_arr = Cmd::createGame((string)@$data['name'], (int)@$connection->user_id); break;
                case 'show_games': $res_arr = Cmd::showGames((int)@$connection->user_id); break;
                case 'join_game': $res_arr = Cmd::joinGame((int)@$data['game_id'], (int)@$connection->user_id); break;
                case 'start_game': $res_arr = Cmd::startGame((int)@$data['game_id'], (int)@$connection->user_id); break;
                case 'quit_game': $res_arr = Cmd::quitGame((int)@$connection->game_id, (int)@$connection->user_id); break;
                case 'play': $res_arr = Cmd::play((int)@$data['state_id'], (array)@$data['cards'], (int)@$data['target'], (int)@$connection->user_id); break;
                case 'draw': $res_arr = Cmd::draw((int)@$data['state_id'], (int)@$connection->user_id); break;
                case 'get_state': $res_arr = Cmd::getState((int)@$connection->user_id); break;
                case 'get_uni_cards': $res_arr = Cmd::getUniCards((int)@$connection->user_id); break;
                case 'get_all_cards': $res_arr = Cmd::getAllCards((int)@$connection->user_id); break;
                default: $res_arr = ['status' => 1];
            }
            
            #If user_id in return
            if (isset($res_arr['user_id'])){
                $connection->user_id = $res_arr['user_id'];
            }
            #If game_id in return
            if (isset($res_arr['game_id'])){
                $connection->game_id = $res_arr['game_id'];
            }
            #
            if ($res_arr['status'] == 0 && $data['cmd'] == 'join_game'){
                $connection->game_id = $data['game_id'];
            }

            $json = json_encode($res_arr);            
            $connection->send($json); // send private response
            
            if ($res_arr['status'] == 0 && ($data['cmd'] == 'start_game' || $data['cmd'] == 'play' || $data['cmd'] == 'draw')){ // sent state to all after successful cmd='play' or cmd='draw'
                foreach($connections as $conn){
                    $res_state = Cmd::getState((int)@$conn->game_id,(int)@$conn->user_id);
                    $json = json_encode($res_state);
                    $conn->send($json);
                }
            }
        }

        public function onClose(ConnectionInterface $conn) {
            $this->clients->detach($conn);
        }

        public function onError(ConnectionInterface $conn, \Exception $e) {
            $conn->close();
        }
    }
?>
