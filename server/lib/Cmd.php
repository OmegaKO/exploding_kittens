<?php
    class Cmd{
        protected static $db;
        
        public static function init(){
            self::$db = DB::getInstance(); // eager loading
        }

        public static function registration($username, $password, $email, $user_id){
            if ($user_id > 0){ // if already logged in
                return ['status' => 11];
            }
            $username = self::$db->escape($username);
            $email = self::$db->escape($email);
            self::$db->lock('users');
            $query = "select id from users where username='$username' or email='$email'";
            self::$db->query($query);
            if (self::$db->getNumRows() > 0){ // if this username or password exist
                self::$db->unlock();
                return ['status' => 12];
            }
            $usrnpasswd = password_hash($username . $password . PASSWORD_SALT, PASSWORD_ARGON2I);
            #Save user
            $query = "insert into users(username,usrnpasswd,email) values('$username','$usrnpasswd','$email')";
            self::$db->query($query);
            $user_id = self::$db->getInsertID();
            self::$db->unlock();
            if ($user_id == 0){ // not inserted by some unknown reason
                return ['status' => 13];
            }
            return ['status' => 0];
        }
        
        public static function login($username, $password, $user_id){
            if ($user_id > 0){ // if already logged in
                return ['status' => 21];
            }
            $username = self::$db->escape($username);
            $query = "select id,usrnpasswd from users where username='$username'";
            $data = self::$db->queryGetArray($query, MYSQL_RES_ROW);
            if (empty($data) || !password_verify($username . $password . PASSWORD_SALT, $data['usrnpasswd'])){ // if user not exist or invalid password
                return ['status' => 22];
            }
            return ['status' => 0, 'user_id' => $data['id']];
        }
        
        public static function passwordRecovery($email, $user_id){
            if ($user_id > 0){ // if already logged in
                return ['status' => 31];
            }
            $to = $email; // needed for send part
            $email = self::$db->escape($email);
            $query = "select id,username from users where email='$email'";
            $data = self::$db->queryGetArray($query, MYSQL_RES_ROW);
            if ($data['id'] == 0){ // not exist
                return ['status' => 0];
            }
            #Gen rand password
            $pass = '';
            for($i = 0; $i < 8; $i ++){
                $pass .= chr(mt_rand(33, 126));
            }
            #Update password in DB
            $usrnpasswd = password_hash($username . $pass . PASSWORD_SALT, PASSWORD_ARGON2I);
            $query = "update users set usrnpasswd='$usrnpasswd' where id=" . $data['id'];
            self::$db->query($query);
            #Send mail
            $subject = 'Възстановяване на забравена парола';
            $message = 'Username: ' . htmlspecialchars($data['username'], ENT_QUOTES, 'UTF-8') . "\nPassword: $pass";
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=UTF-8';
            $headers[] = 'To: ' . $to;
            $headers[] = 'From: ' . ADMIN_EMAIL;
            mail($to, $subject, $message, implode("\r\n", $headers));
            return ['status' => 0]; // success
        }
        
        public static function createGame($name, $user_id){
            if ($user_id == 0){ // if you are not logged in
                return ['status' => 41];
            }
            $name = self::$db->escape($name);
            self::$db->lock('games');
            $query = "select id from games where winner_id is null and name='$name'";
            self::$db->query($query);
            if (self::$db->getNumRows() > 0){ // if active game with this name exist
                self::$db->unlock();
                return ['status' => 42];
            }
            $query = "insert into games(name,game_owner_id) values('$name',$user_id)";
            self::$db->query($query);
            $game_id = self::$db->getInsertID();
            self::$db->unlock();
            if ($game_id == 0){ // if by some unknown reason game is not created
                return ['status' => 43];
            }
            self::joinGame($game_id, $user_id); // after create auto join
            return ['status' => 0, 'game_id' => $game_id];
        }
        
        public static function showGames($user_id){
            if ($user_id == 0){ // if you are not logged in
                return ['status' => 51];
            }
            #Set zombie status to games
            $query = "update games set status=3 where status=0 UNIX_TIMESTAMP(NOW)-" . GAME_ZOMBIE_TIMEOUT . ">UNIX_TIMESTAMP(game_created)";
            self::$db->query($query);
            $query = "update games set status=4 where status=1 UNIX_TIMESTAMP(NOW)-" . GAME_ZOMBIE_TIMEOUT . ">UNIX_TIMESTAMP(game_started)";
            self::$db->query($query);
            #Load games
            $query = "select id,name,count(t1.id) as players from games t1 left join games2users t2 on(t1.id=t2.game_id) where t1.status=0 group by t2.game_id";
            $games = self::$db->queryGetArray($query);
            return ['status' => 0, 'games' => $games];
        }
        
        public static function joinGame($game_id, $user_id){
            if ($user_id == 0){ // if you are not logged in
                return ['status' => 61];
            }
            self::$db->lock(['games', 'games2users']);
            $game_data = self::getGameData($game_id);
            if ($game_data['status'] != 0){ // check if this game is created
                self::$db->unlock();
                return ['status' => 62];
            }
            if (self::isUserInTheGame($game_id, $user_id)){ // check if this user is already in the game
                self::$db->unlock();
                return ['status' => 64];
            }
            if (self::getPlayersNum($game_id) > 4){ // check for enough players (>4)
                self::$db->unlock();
                return ['status' => 65];
            }
            #Insert me into the game
            $query = "insert into games2users(game_id,user_id) values($game_id, $user_id)";
            self::$db->query($query);
            self::$db->unlock();
            return ['status' => 0];
        }
        
        public static function startGame($game_id, $user_id){
            if ($user_id == 0){ // if you are not logged in
                return ['status' => 71];
            }
            self::$db->lock(['games', 'games2users']);
            #Check is this user a owner
            $query = "select id from games where game_id=$game_id and game_owner_id=$user_id";
            self::$db->query($query);
            if (self::$db->getNumRows() == 0){ // if you are no owner of this game or this game not exist
                self::$db->unlock();
                return ['status' => 72];
            }
            #
            if (self::getGameStatus($game_id) != 0){ // check if this game is created
                self::$db->unlock();
                return ['status' => 73];
            }
            self::$db->lock('games');
            $n = self::$getPlayersNum($game);
            if ($n < 2 || $n > 5){ // if wrong player number
                self::$unlock();
                return ['status' => 74];
            }
            #Start the game
            $query = "update games set status=1 where game_id=$game_id";
            self::$db->query($query);
            self::$db->unlock();
            #Give cards to players
            //TODO
            return ['status' => 0];
        }
        
        public static function quitGame($game_id, $user_id){
            if ($user_id == 0){ // if you are not logged in
                return ['status' => 81];
            }
            #Delete user from the game
            $query = "delete from games2users where game_id=$game_id and user_id=$user_id";
            self::$db->query($query);
            #If game is not started and the quiter is the owner => delete the game
            $game_data = self::getGameData($game_id);
            if ($game_data['status'] == 0 && $game_data['game_owner_id'] == $user_id){
                $query = "delete from games where id=$game_id";
                self::$db->query($query);
            }
            else{ // if game is started => change ownership of his/her cards to InPlay (user_id=1)
                $query = "update games2cards set owner_id=1 where game_id=$game_id and owner_id=$user_id";
                self::$db->query($query);
            }
            return ['status' => 0];
        }
        
        #FOR INNER USE
        
        protected static function getGameData($game_id){
            $query = "select * from games where id=$game_id";
            return self::$db->queryGetArray($query, MYSQL_RES_ROW);
        }
        
        protected static function getPlayersNum($game_id){
            $query = "select count(user_id) from games2users where game_id=$game_id";
            return self::$db->queryGetArray($query, MYSQL_RES_VAL);
        }
        
        protected static function isUserInTheGame($game_id, $user_id){
            $query = "select count(user_id) from games2users where game_id=$game_id and user_id=$user_id";
            return (bool)self::$db->queryGetArray($query, MYSQL_RES_VAL);
        }
    }
?>
