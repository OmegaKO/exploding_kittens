let socket = new WebSocket("ws://localhost:8000");

socket.onopen = function(e) {
    // nothing
};

socket.onmessage = function(event) {
  document.getElementById('out').value = event.data;
};

socket.onclose = function(event) {
  if (event.wasClean) {
    alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
  } else {
    alert('[close] Connection died');
  }
};

socket.onerror = function(error) {
  alert(`[error] ${error.message}`);
};

function sendCmd() {
    $cmd = document.getElementById('in').value;
    socket.send($cmd);
}
