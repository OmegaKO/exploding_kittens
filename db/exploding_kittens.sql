DROP TABLE IF EXISTS `cards`;
CREATE TABLE IF NOT EXISTS `cards` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `card_type_id` tinyint(3) unsigned NOT NULL,
  `call` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cards_card_types` (`card_type_id`),
  CONSTRAINT `FK_cards_card_types` FOREIGN KEY (`card_type_id`) REFERENCES `card_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `card_types`;
CREATE TABLE IF NOT EXISTS `card_types` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `games`;
CREATE TABLE IF NOT EXISTS `games` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `game_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `game_started` timestamp NULL DEFAULT NULL,
  `game_owner_id` int(10) unsigned NOT NULL,
  `state_id` int(10) unsigned NOT NULL DEFAULT 0,
  `turn_owner_id` int(10) unsigned DEFAULT NULL,
  `end_turn` int(10) unsigned DEFAULT NULL COMMENT 'unix_time',
  `interrupter_id` int(10) unsigned DEFAULT NULL,
  `interrupt_end` int(10) unsigned DEFAULT NULL,
  `game_finished` timestamp NULL DEFAULT NULL,
  `winner_id` int(10) unsigned DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT '0 - created, 1 - started, 2 - finished, 3 - zombie (created), 4 - zombie (started)',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `FK_games_users_2` (`turn_owner_id`),
  KEY `FK_games_users_3` (`interrupter_id`),
  KEY `FK_games_users_4` (`winner_id`),
  KEY `FK_games_users` (`game_owner_id`),
  CONSTRAINT `FK_games_users` FOREIGN KEY (`game_owner_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_games_users_2` FOREIGN KEY (`turn_owner_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_games_users_3` FOREIGN KEY (`interrupter_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_games_users_4` FOREIGN KEY (`winner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `games2cards`;
CREATE TABLE IF NOT EXISTS `games2cards` (
  `game_id` bigint(20) unsigned NOT NULL,
  `card_id` tinyint(3) unsigned NOT NULL,
  `owner_id` int(10) unsigned NOT NULL COMMENT '0 - InGame, 1 - Played',
  `order` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`game_id`,`card_id`),
  KEY `FK_games2cards_cards` (`card_id`),
  KEY `FK_games2cards_users` (`owner_id`),
  CONSTRAINT `FK_games2cards_cards` FOREIGN KEY (`card_id`) REFERENCES `cards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_games2cards_games` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_games2cards_users` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `games2users`;
CREATE TABLE IF NOT EXISTS `games2users` (
  `game_id` bigint(20) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `order` tinyint(3) unsigned DEFAULT NULL,
  `turns` tinyint(3) unsigned NOT NULL DEFAULT 1 COMMENT 'turns at once',
  PRIMARY KEY (`game_id`,`user_id`),
  KEY `FK_games2users_users` (`user_id`),
  CONSTRAINT `FK_games2users_games` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_games2users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `usrnpasswd` varchar(128) COMMENT 'algo: argon2i(username . password . salt?)',
  `email` varchar(128),
  `registered` timestamp NULL DEFAULT current_timestamp(),
  `last_login` timestamp NULL DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT '0 - registered, 1 - blocked/banned, 2 - deleted',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `usrnpasswd`, `email`, `registered`, `last_login`, `status`) VALUES
	(1, 'Played', NULL, NULL, NULL, NULL, 0),
	(2, 'InGame', NULL, NULL, NULL, NULL, 0);
