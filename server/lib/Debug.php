<?php
    class Debug{
        public static function log($message, $log_type = 'error'){
            @mkdir('log' . DIRECTORY_SEPARATOR . $log_type);
            $f_name = 'log' . DIRECTORY_SEPARATOR . $log_type . DIRECTORY_SEPARATOR . date('Y-m-d') . '.log';
            Files::saveFile($f_name, date('h:i:s') . ' - ' . (string)@$_SERVER['REMOTE_ADDR'] . ' - ' . $message . "\n", true);
        }
    }
?>
